# Python Backend for Arduino based In-Circuit Tester (ICT) 

The frontend is a QT5.0 based Python application that allows the user to interact with the FCT. Using the frontend, one can allocate the source signal (audio signal or LFO) against which each test point on the PCB can be analyzed. The graphical view can visually gauge the output at these test points with respect to the set source signal. This makes it convenient for a reliable determination of their validity.

Additionally, the frontend also acts as an interacting layer between the user and the [backend](https://gitlab.com/ict_afa/backend). The desired frequency and waveforms for the audio and LFO sources are communicated to the backend via the frontend. Additionally, the visual output that the frontend plots is received from the backend.

<p align="center">
  <img src="docs/frontend.png" />
</p>

## Installation

1.  Clone this repo to local, switch to repo

```sh
git clone git@gitlab.com:ict_afa/frontend.git

cd frontend
```

2. Create a virtual environment inside repo

```sh
python -m venv venv 
```

3. Activate venv

```sh
# for bash-compatible shells 
source venv/bin/activate

# for fish, csh, etc use appropriate activation scripts inside venv/bin/
```

4. Install required python dependencies

```sh
#upgrade pip if required
pip install --upgrade pip

#install all requirements
pip install -r requirements.txt
```

## Usage

### Linux
Run `main.py`

```sh
python main.py 
```
### Interacting with the GUI

Using the text input, the desired frequency can be sent to the backend. The waveform type can be one among sine, square, sawtooth or triangle. After confirming the two selections, click send to communicate it with the backends.

The `I/O Pin Mapper` can be used to set the source against which the signal obtained at each analog pin (these come from the test point on the PCB) is compared.

The graph plots the output received by each analog pin as well as the source to which it was mapped. To visualize the output at a particular pin, click on the corresponding graph tab.

_Note: Audio signals are coded yellow, LFOs are blue while the output is red and dotted._

## Contributing

0. [Report Bugs or Request Features](https://gitlab.com/ict_afa/frontend/-/issues)
1. Fork it (<https://gitlab.com/ict_afa/frontend>)
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Merge/Pull Request

## License

Distributed under the GNU General Public License v3.0 License. See `LICENSE` for more information.
