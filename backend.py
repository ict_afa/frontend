from scipy import signal

pin_names = list()
instance_dict = dict()
input_curve_names = ['lfo', 'wave']

MAX_BUFFER_SIZE = 256


def add_instance(instance_type, instance):
    instance_dict[instance_type] = instance


def remove_graph_tab(row_name):
    instance_dict['Graph'].remove_tab(row_name.strip())


def set_graph_tab(row_name, src):
    instance_dict['Graph'].set_new_tab(graph_name=row_name.strip(), src=src)


def add_graph_curve(graph_name):
    instance_dict['Graph'].tabs[graph_name.strip()].add_plot()


def update_graph(graph_name, curve_name, y, choice='append'):
    if choice == 'append':
        instance_dict['Graph'].tabs[graph_name.strip()].append_graph(src=curve_name, element_y=y)
    elif choice == 'set':
        instance_dict['Graph'].tabs[graph_name.strip()].set_graph(src=curve_name, y=y)


def send_graph_data(graph_name, send_input=False, send_output=False):
    # ? Called By:
    # ? qt_taber => GraphTaber::tabChange -> SIGNAL
    # ? qt_mapTabler => Radiodemo::btnState -> SIGNAL

    import numpy as np

    t = np.linspace(0, 1, 100, False)  # 1 second
    samples = np.sin(2 * np.pi * np.random.uniform(1, 10) * t)
    sos = signal.butter(1, 5, 'hp', fs=100, output='sos')
    samples2 = signal.sosfilt(sos, samples)

    for each_samp, each_filtered_samp in zip(samples, samples2):
        if send_input:
            update_graph(graph_name, curve_name='input', y=each_samp, choice='append')
        if send_output:
            update_graph(graph_name, curve_name='output', y=each_filtered_samp, choice='append')
