import sys, backend
from PySide2.QtGui import QIntValidator, QPalette, QColor
from PySide2.QtWidgets import QApplication, QPushButton, QWidget, QLineEdit, QFormLayout, QComboBox, QGroupBox, \
    QVBoxLayout, QHBoxLayout
from PySide2.QtCore import Qt, Slot
from qt_taber import GraphTaber
from qt_mapTabler import Mapper


# TODO: Link serial data with front end

class OSC_Control(QGroupBox):

    def __init__(self, control_name):
        super(OSC_Control, self).__init__(control_name.strip())

        self.sim_frequency_entry = QLineEdit()
        self.sim_frequency_entry.setValidator(QIntValidator(1, 50000))
        self.sim_frequency_entry.setText("10")
        self.sim_frequency_entry.setPlaceholderText("Specify Frequency(in Hz)")
        self.sim_frequency_entry.setAlignment(Qt.AlignCenter)

        self.sim_select_wform = QComboBox()
        wform_list = ["Sine", "Square", "Triangle", "Sawtooth"]
        for item_no, wform in enumerate(wform_list):
            self.sim_select_wform.addItem(wform)
            self.sim_select_wform.setItemData(item_no, Qt.AlignCenter, Qt.TextAlignmentRole)

        self.confirm_sim_params_button = QPushButton("Send")
        self.confirm_sim_params_button.clicked.connect(self.send_sim_data)

        sim_send_form = QFormLayout()
        sim_send_form.addRow("Frequency", self.sim_frequency_entry)
        sim_send_form.addRow("Waveform Type", self.sim_select_wform)
        sim_send_form.addRow(None, self.confirm_sim_params_button)

        self.setLayout(sim_send_form)

    @Slot()
    def send_sim_data(self):
        print("Sent: {} Wave of {}Hz".format(self.sim_select_wform.currentText(), self.sim_frequency_entry.text()))


if __name__ == '__main__':
    backend.pin_names = list()
    pin_count = 10
    for i in range(pin_count):
        backend.pin_names.append("Pin A{}".format(i))

    app = QApplication(sys.argv)

    app.setStyle("Fusion")

    palette = QPalette()
    palette.setColor(QPalette.Window, QColor(53, 53, 53))
    palette.setColor(QPalette.WindowText, Qt.white)
    palette.setColor(QPalette.Base, QColor(25, 25, 25))
    palette.setColor(QPalette.AlternateBase, QColor(53, 53, 53))
    palette.setColor(QPalette.ToolTipBase, Qt.black)
    palette.setColor(QPalette.ToolTipText, Qt.white)
    palette.setColor(QPalette.Text, Qt.white)
    palette.setColor(QPalette.Button, QColor(53, 53, 53))
    palette.setColor(QPalette.ButtonText, Qt.white)
    palette.setColor(QPalette.BrightText, Qt.red)
    palette.setColor(QPalette.Link, QColor(42, 130, 218))
    palette.setColor(QPalette.Highlight, QColor(42, 130, 218))
    palette.setColor(QPalette.HighlightedText, Qt.black)
    app.setPalette(palette)

    base_widget = QWidget()

    graphs = GraphTaber()
    backend.add_instance('Graph', graphs)

    mainLayout = QVBoxLayout()

    oscLayout = QHBoxLayout()
    oscLayout.addWidget(OSC_Control("Wave Simulator"))
    oscLayout.addWidget(OSC_Control("LFO Simulator"))
    oscLayout.addWidget(Mapper(name='I/O Pin Mapper'))

    graphs.blockSignals(False)
    # graphs.tabChange(0)  # ? To display data in default(first) tab upon application launch

    mainLayout.addLayout(oscLayout)
    mainLayout.addWidget(graphs)

    base_widget.setLayout(mainLayout)
    base_widget.setWindowTitle("Animal Factory Amplification FCT v0.1")
    base_widget.show()

    app.exec_()
