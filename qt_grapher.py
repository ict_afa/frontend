from PySide2.QtGui import Qt
from PySide2.QtWidgets import QVBoxLayout, QWidget
import pyqtgraph as pg
import random, backend


class Grapher(QWidget):


    def __init__(self, input_src):
        super(Grapher, self).__init__()

        self.graphWidget = pg.PlotWidget()
        graph_layout = QVBoxLayout()
        graph_layout.addWidget(self.graphWidget)
        self.setLayout(graph_layout)

        self.curves = dict()
        self.assigned_input = False
        self.assigned_other = False

        self.input_src = input_src
        self.set_plot(src=input_src)

    def set_graph(self, src, y):
        if src == 'input':
            src = self.input_src

        if src not in self.curves.keys():
            print("Previously unassigned src = {}: Generating new curve...".format(src))
            self.set_plot(src.strip())

        self.curves[src][1] = y
        self.curves[src][0].setData(self.curves[src][1])
        pg.QtGui.QApplication.processEvents()  # ? Wait till new data can be added

    def append_graph(self, src, element_y):
        global MAX_BUFFER_SIZE

        if src == 'input':
            src = self.input_src

        if src not in self.curves.keys():
            print("Invalid src = {}: Generating new curve...".format(src))
            self.set_plot(src.strip())

        self.curves[src][1].append(element_y)
        if len(self.curves[src][1]) > backend.MAX_BUFFER_SIZE:
            self.curves[src][1] = self.curves[src][1][1:]
        self.curves[src][0].setData(self.curves[src][1])
        pg.QtGui.QApplication.processEvents()  # ? Wait till new data can be added

    def set_plot(self, src):
        if src not in self.curves.keys():
            if src in backend.input_curve_names:
                if self.assigned_input:
                    self.graphWidget.removeItem(self.curves[self.input_src][0].clear())
                    del self.curves[self.input_src]
                self.input_src = src
                self.assigned_input = True
            else:
                self.assigned_other = True
            print("Linked {} to graph".format(src))
        else:
            del self.curves[src]

        if src == 'output':
            pen = pg.mkPen(color='r', style=Qt.DotLine, width=2)
        elif src == 'lfo':
            pen = pg.mkPen(color='b')
        elif src == 'wave':
            pen = pg.mkPen(color='y')
        else:
            pen = pg.mkPen(color=(random.randint(0, 255), random.randint(0, 255), random.randint(0, 255)))

        self.curves[src] = [self.graphWidget.plot(pen=pen), list()]
