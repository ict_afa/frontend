import backend
from PySide2.QtWidgets import QHBoxLayout, QRadioButton, QVBoxLayout, QLabel, QGroupBox


class Radiodemo(QHBoxLayout):

    def __init__(self, parent=None, row_name='Untitled'):
        super(Radiodemo, self).__init__(parent)

        self.rowName = row_name.strip()

        self.addWidget(QLabel(self.rowName))

        self.b1 = QRadioButton("")
        self.b1.setChecked(True)
        self.b2 = QRadioButton("")

        self.addWidget(self.b1)
        self.addWidget(self.b2)

        self.btnstate(1)

        self.b1.toggled.connect(lambda: self.btnstate(1))
        self.b2.toggled.connect(lambda: self.btnstate(2))

    def btnstate(self, select):
        if select == 1:
            self.b1.setAutoExclusive(False)
            self.b2.setAutoExclusive(False)
            if self.b1.isChecked():
                print("Wave simulator assigned to {}".format(self.rowName))
                self.b2.setChecked(False)
                backend.set_graph_tab(self.rowName, src='wave')
                backend.send_graph_data(self.rowName, send_input=True)
        elif select == 2:
            self.b1.setAutoExclusive(False)
            self.b2.setAutoExclusive(False)
            if self.b2.isChecked():
                print("LFO simulator assigned to {}".format(self.rowName))
                self.b1.setChecked(False)
                backend.set_graph_tab(self.rowName, src='lfo')
                backend.send_graph_data(self.rowName, send_input=True)

        if not self.b1.isChecked() and not self.b2.isChecked():
            backend.remove_graph_tab(self.rowName)


class Mapper(QGroupBox):
    def __init__(self, entry_names=None, name="I/O Mapper"):
        super(Mapper, self).__init__(name.strip())
        self.mapBox = QVBoxLayout()
        mapLabels = QHBoxLayout()

        mapLabels.addWidget(QLabel("Input Pin"))
        mapLabels.addWidget(QLabel("Wave Simulator"))
        mapLabels.addWidget(QLabel("LFO Simulator"))
        self.mapBox.addLayout(mapLabels)

        if entry_names == None:
            entry_names = backend.pin_names

        for each_entry in entry_names:
            self.mapBox.addLayout(Radiodemo(row_name=each_entry))

        self.setLayout(self.mapBox)
