from PySide2.QtCore import Slot, QTimer
from PySide2.QtWidgets import QTabWidget
from qt_grapher import Grapher
import backend


class GraphTaber(QTabWidget):
    def __init__(self, parent=None):
        super(GraphTaber, self).__init__(parent)
        self.tabs = dict()
        self.blockSignals(True)
        self.currentChanged.connect(self.tabChange)

    @Slot()
    def tabChange(self, i):
        print("Active graph tab set to tab index: {}".format(i))
        while True:
            backend.send_graph_data(backend.pin_names[i], send_input=True, send_output=True)

    def set_new_tab(self, src, graph_name='Untitled'):
        if graph_name not in self.tabs.keys():
            self.tabs[graph_name] = Grapher(input_src=src)
            self.setCurrentIndex(-1)
            self.insertTab(backend.pin_names.index(graph_name), self.tabs[graph_name], graph_name)
        else:
            self.tabs[graph_name].set_plot(src=src)

    def remove_tab(self, graph_name):
        print("Removed Tab: {}".format(graph_name))
        self.removeTab(backend.pin_names.index(graph_name))
        del self.tabs[graph_name]
