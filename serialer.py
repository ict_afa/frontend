from serial import Serial
import backend
import re

ser = Serial()

generator_pins = {'lfo': 0, 'wave': 1}
generator_waveforms = {'sine': 0, 'square': 1, 'triangle': 2, 'sawtooth': 3}


def initialize():
    global ser
    ser = Serial(port='/dev/ttyACM0', baudrate=2000000)
    print("[SERIAL] => Connected to MCU")


def send_signal_generator_info(generator_type, signal_waveform, signal_frequency):
    global ser
    if generator_type not in generator_pins.keys():
        print("[!SERIAL:ERROR] => Invalid generator pin type = {}".format(generator_type))
        return -1
    elif signal_waveform not in generator_waveforms.keys():
        print("[!SERIAL:ERROR] => Invalid generator waveform type = {}".format(signal_waveform))
        return -1
    elif signal_frequency < 0 or signal_frequency > 20000:
        print("[!SERIAL:ERROR] => Invalid generator frequency = {}Hz".format(signal_frequency))
        return -1

    msg = "{}{}{}".format(signal_frequency, generator_waveforms[signal_waveform], generator_pins[generator_type])
    ser.write(msg)
    print("[SERIAL->{}] => Successfully sent data to MCU: {} waveform of {}Hz as {} signal".format(msg, signal_waveform,
                                                                                                   signal_frequency,
                                                                                                   generator_type))
    return 0


def set_active_input_pin(input_pin):
    global ser
    pin_number = re.compile(r'Pin A\d+').search(input_pin)
    msg = "-{}".format(pin_number)

    ser.write(msg)
    print("[SERIAL->{}] => Successfully sent data to MCU: Active input pin set to Pin A{}".format(msg, pin_number))


def receive_pin_data(input_pin):
    global ser
    if input_pin not in backend.pin_names:
        print("[!SERIAL:ERROR] => Invalid pin = {}".format(input_pin))
        return -1

    set_active_input_pin(input_pin)

    while True:
        read_data = str(ser.readline(), 'utf-8')
        if read_data == '~~~':
            # TODO: Write code to read in MCU data
            pass
